

### Deploy em produção



Criar o diretorio da aplicação 

```
mkdir /var/www/app/ && cd /var/www/app/
```

Set ambiente como de produção

```
bundle config set deployment 'true' && bundle install
```

Criar/alterar arquivo de configuraçao do Passenger

```
vim Passengerfile.json
```

Execute Passenger 

```
sudo bundle exec passenger start
```
