require 'sinatra'

class RootApp < Sinatra::Base
  get '/' do

    addr_infos = Socket.ip_address_list
    addr_infos.each do |addr_info|
      puts addr_info.ip_address
    end

    addr_infos.to_s
  end
end
